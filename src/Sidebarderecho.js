import React from 'react'
import styled from "styled-components";
import ComponentA from './ComponentA'
import corona from "./assets/images/corona.png";
import correo from "./assets/images/correo.png";
import search from "./assets/images/searching.png";
const Container =styled.div`
width: 30%;
`
export const Sidebarderecho = () => {
  const data=[{
    icon:corona ,colorIcon:"white" ,title:"Paquete premium" ,subtitle:"Descubre nuevas funciones" ,enlace:"Hazte Premium"
  },
  {
    icon:search ,colorIcon:"white" ,title:"Dominio" ,subtitle:"Registra tu propio dominio, p . ej. www.mipaginaweb.com" ,enlace:"Registrar un nuevo dominio"
  },
  {
    icon:correo ,colorIcon:"white" ,title:"Cuentas de email" ,subtitle:"Envia correos profesionales" ,enlace:"Crear nueva cuenta de email"
  }
]
  return (
  <Container>
    {
    data.map((v,i)=><ComponentA {...v}/>)
    }
  </Container>
   
  )
}

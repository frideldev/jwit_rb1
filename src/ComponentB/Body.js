import React from 'react'
import styled from 'styled-components'
import { Title } from '../ComponentA/Body'
const Container =styled.div`
width: 85%;
padding-left: 5px;
display: flex;
flex-direction: row;
justify-content: right;
align-items: center;
`
const Enlace =styled.img`
height: 25px;
padding-left: 150px;
`
export const Body = (params) => {
  return (
   <Container>
    <Title>
      {params.title}
    </Title>
    <Enlace src={params.enlace}/>
   </Container>
  )
}

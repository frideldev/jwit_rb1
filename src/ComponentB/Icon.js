import styled from "styled-components";

const Container =styled.div`
width: 15%;

`
const Body =styled.div`
background-color: ${props=>props.colorIcon};
height: 100%;
width: 100%;
border-radius: 25px;
display: flex;
justify-content: center;
align-items: center;
`
const Iconimg =styled.img`
height: 45px;

`
const Icon=({ico})=> {
  return (
    <Container >
        <Body >
        <Iconimg  src={ico}/>
        </Body>
    </Container>
  );
}

export default Icon;

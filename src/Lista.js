import React from 'react'
import styled from "styled-components";
import ComponentB from './ComponentB'

const Container =styled.div`
width: 90%;
`
export const Lista = ({lista}) => {
  
  return (
  <Container>
    {
      lista.map((v,i)=><ComponentB {...v}/>)
    }
  </Container>
   
  )
}
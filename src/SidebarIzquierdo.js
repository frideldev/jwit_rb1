import React from 'react'
import styled from "styled-components";
import { Lista } from './Lista';
import { Menu } from './Menu';
import mano from "./assets/images/mano.png";
import analitica from "./assets/images/analitica.png";
import descuento from "./assets/images/descuento.png";
import formulario from "./assets/images/formulario.png";
import carrito from "./assets/images/carrito.png";
const Container =styled.div`
width: 70%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
`
export const Sidebarizquierdo = () => {
  const menu =[
    {title:"Resumen"},
    {title:"Administracion"},
    {title:"Configuracion de tienda online"}
]
const lista =[
  {icon:carrito , title:"Pedidos" , enlace:"Hazte Premium"},
  {icon:analitica , title:"Analitica de Negocio" , enlace:"Registrar un nuevo dominio"},
  {icon:mano , title:"Clientes" , enlace:"Crear nueva cuenta de email"},
  {icon:descuento , title:"Descuentos" , enlace:"Crear nueva cuenta de email"},
  {icon:formulario , title:"Formularios recibidos" , enlace:"Crear nueva cuenta de email"}

]
  return (
  <Container>
    <Menu menu={menu}/>
    <Lista lista={lista}/>
  </Container>
   
  )
}

import React from "react";
import Icon from "./Icon";
import { Body } from "./Body";
import styled from "styled-components";

const Container =styled.div`
height: 80px;
width: 350px;
display: flex;
flex-direction: row;
margin-top: 15px;
`
const ComponentA = (params) => {
  return (
    <Container >
     <Icon ico={params.icon} colorIcon={params.colorIcon}/>
     <Body title={params.title} subtitle={params.subtitle} enlace={params.enlace}/>
    
  </Container>
  )
}
export default ComponentA;

import styled from "styled-components";
import { Lista } from "./Lista";
import { Sidebarderecho } from "./Sidebarderecho";
import { Sidebarizquierdo } from "./SidebarIzquierdo";

const Container =styled.div`
display: flex;
flex-direction: row;
`
const App=()=> {
  return (
    <Container >
    <Sidebarizquierdo/>
    <Sidebarderecho/>
    </Container>
  );
}

export default App;
